import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'JS';
  task = '';

  tasks = [
    {task: 'Buy milk'},
    {task: 'Walk with dog'},
    {task: 'Do homework'},
  ];

  onAddUTask(event: Event) {
    event.preventDefault();
    this.tasks.push({
      task: this.task,
    });
  }
  changeTask(index: number, newTask: string) {
    this.tasks[index].task = newTask;
  }

  onDeletePerson(i: number) {
    this.tasks.splice(i, 1);
  }
}
