import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent  {
  @Input() task = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();
  focusInput = false;

  onDeleteClick() {
    this.delete.emit();
  }

  focus() {
    this.focusInput = !this.focusInput;
  }

  blur() {
    this.focus();
  }

  getInputClass() {
    let className = ''
    if (this.focusInput) {
      className = 'green'
    }
    return 'input ' + className;
  }
  onTaskInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }
}
